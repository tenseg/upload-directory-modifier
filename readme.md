# Upload Directory Modifier

Substitute a string for the blog ID in the upload URL of WordPress network subsites.

**Contributors:** [Tenseg LLC](https://tenseg.net)  
**Requires at least:** 4.5  
**Tested up to:** 5.3  
**Requires PHP:** 5.6

## Description ##

Use `wp --url=https://site.url option add tg_upload_dir dirname` to force this site to use wp-content/uploads/dirname as its uploads directory. You can create the corresponding directory by going to the uploads directory and then `ln -s sites/3 dirname` replacing 3 with the correct blog ID. Note, changing this after the site has been operating will break all existing media unless you do a global find and replace. This plugin is best network activated.

## Changelog ##

### v.1.1, 24 February 2020

Initial release.


