<?php
/*
Plugin Name: Upload Directory Modifier
Plugin URI: 
Description: Substitute a string for the blog ID in the upload URL of WordPress network subsites. Use `wp --url=https://site.url option add tg_upload_dir dirname` to force this site to use wp-content/uploads/dirname as its uploads directory. You can create the corresponding directory by going to the uploads directory and then `ln -s sites/3 dirname` replacing 3 with the correct blog ID. Note, changing this after the site has been operating will break all existing media unless you do a global find and replace. This plugin is best network activated.
Version: 1.1
Author: Tenseg LLC
Author URI: http://tenseg.net/
License: Simplified BSD License (https://opensource.org/licenses/BSD-2-Clause)
*/

/*
	see https://stackoverflow.com/a/29941641/383737
*/

add_filter( 'upload_dir', 'tg_upload_dir_filter' );

function tg_upload_dir_filter( $dirs ) {
	global $wpdb;
	
	$directory = get_option('tg_upload_dir');
	
	if (! $directory) {
		return $dirs;
	}
		
	$dirs['baseurl'] = site_url() . '/wp-content/uploads/' . $directory;
	$dirs['basedir'] = ABSPATH . 'wp-content/uploads/' . $directory;	
	$dirs['path'] = $dirs['basedir'] . $dirs['subdir'];
	$dirs['url']  = $dirs['baseurl'] . $dirs['subdir'];
	
	return $dirs;
}
